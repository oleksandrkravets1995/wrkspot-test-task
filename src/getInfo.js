import axios from 'axios'

export const getInfo = async (amount) => {
    const URL = `https://cat-fact.herokuapp.com/facts/random?animal_type=cat&amount=${amount}`;
  
    try {
      const res = await axios.get(URL);
      return res;
    } catch (error) {
      console.log(error);
    }
  }