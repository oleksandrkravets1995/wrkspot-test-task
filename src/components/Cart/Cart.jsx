import { format } from "date-fns";
import PropTypes from "prop-types";

export const Cart = ({ text, createdAt }) => {
  const date = format(new Date(createdAt), 'MM/dd/yyyy');
  return (
    <>
    <tbody>
    <tr data-testid="cart">
      <td className="created-data-td">
        {date}
      </td>
      <td className="text-data-td">{text}</td>
    </tr>
    </tbody>
    </>
  );
};

Cart.propTypes = {
  text: PropTypes.string,
  createdAt: PropTypes.string,
};
