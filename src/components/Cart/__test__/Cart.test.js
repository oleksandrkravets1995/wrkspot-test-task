import { render, screen, cleanup } from "@testing-library/react";
import { Cart } from "../Cart";

afterEach(() => {
  cleanup();
});

test("should render Cart component", () => {
  render(<Cart text="Some text" createdAt={"2018-01-04T01:10:54.673Z"} />);
  const emptyComponentElement = screen.getByTestId("cart");
  expect(emptyComponentElement).toBeInTheDocument();
  expect(emptyComponentElement).toHaveTextContent("Some text");
  expect(emptyComponentElement).toContainHTML("td");
  expect(emptyComponentElement).toContainHTML("tr");
  expect(emptyComponentElement).toHaveTextContent("01/04/2018");
});

test("should render Cart component with another props", () => {
  render(<Cart text="Some text 123" createdAt={"2018-02-04T01:10:54.673Z"} />);
  const emptyComponentElement = screen.getByTestId("cart");
  expect(emptyComponentElement).toBeInTheDocument();
  expect(emptyComponentElement).toHaveTextContent("Some text 123");
  expect(emptyComponentElement).toContainHTML("td");
  expect(emptyComponentElement).toContainHTML("tr");
  expect(emptyComponentElement).toHaveTextContent("02/04/2018");
});
