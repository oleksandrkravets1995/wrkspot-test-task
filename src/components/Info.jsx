import React, { useState, useEffect } from "react";
import axios from "axios";

import { apiUrl } from "../const/apiUrl";
import { Cart } from "./Cart/Cart";
import { SearchCountInput } from "./CountInput/CountInput";
import { EmptyComponent } from "./EmptyComponent/EmptyComponent";

import "../App.css";

export const Info = () => {
  const [amount, setAmount] = useState(0);
  const [info, setInfo] = useState([]);
  const [loading, setLoading] = useState(true);

  const fetchData = async (num) => {
    setLoading(true);
    const result = await axios(`${apiUrl}${num}`);

    setInfo(result.data);
    setLoading(false);
  };

  useEffect(() => {
    fetchData(amount);
  }, []);

  const getData = async () => {
    await fetchData(amount);
    setAmount(0);
  };

  const items = Array.isArray(info) ? info : [info];

  if (loading) {
    return (
      <div class="lds-circle">
        <div></div>
      </div>
    );
  }
  return (
    <div className="column">
      <div className="fixed-input">
        <SearchCountInput
          setAmount={setAmount}
          amount={amount}
          loading={loading}
        />
        <button
          className="search-count-submit"
          disabled={loading}
          onClick={() => getData()}
        >
          Click
        </button>
      </div>
      {!!items.length ? (
        <>
          <table border="1" className="table-info">
            <thead>
              <tr>
                <th>Created at</th>
                <th>Text</th>
              </tr>
            </thead>
            {items.map(({ id, createdAt, text }) => (
              <Cart key={id} text={text} createdAt={createdAt} />
            ))}
          </table>
        </>
      ) : (
        <EmptyComponent text="Please type number of cats that you would like get" />
      )}
    </div>
  );
};
