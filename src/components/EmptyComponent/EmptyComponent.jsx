import PropTypes from "prop-types";

export const EmptyComponent = ({ text }) => {
  return <div className="center" data-testid="empty">{text}</div>;
};

EmptyComponent.propTypes = {
  text: PropTypes.string,
};
