import { render, screen, cleanup } from "@testing-library/react";
import { EmptyComponent } from "../EmptyComponent";

afterEach(() => {
    cleanup()
})

test("should render EmptyComponent component", () => {
  render(
    <EmptyComponent text="Please type number of cats that you would like get" />
  );
  const emptyComponentElement = screen.getByTestId("empty");
  expect(emptyComponentElement).toBeInTheDocument();
  expect(emptyComponentElement).toHaveTextContent(
    "Please type number of cats that you would like get"
  );
});

test("should render EmptyComponent with another text", () => {
    render(
      <EmptyComponent text="Empty component text" />
    );
    const emptyComponentElement = screen.getByTestId("empty");
    expect(emptyComponentElement).toBeInTheDocument();
    expect(emptyComponentElement).toHaveTextContent(
      "Empty component text"
    );
  });