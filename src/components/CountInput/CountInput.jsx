import React from "react";
import PropTypes from "prop-types";

export const SearchCountInput = React.memo(({ setAmount, amount, loading }) => {
  return (
    <input
      data-testid="count-input"
      type="number"
      min="0"
      max="500"
      placeholder="Type the number"
      onChange={(e) => setAmount(e.target.value)}
      value={amount}
      disabled={loading}
      className="count-input"
    />
  );
});

SearchCountInput.propTypes = {
  amount: PropTypes.string,
  loading: PropTypes.bool,
  setAmount: PropTypes.func,
};
