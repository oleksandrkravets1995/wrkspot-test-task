import { render, screen, cleanup } from "@testing-library/react";
import { SearchCountInput } from "../CountInput";

afterEach(() => {
  cleanup();
});

test("should render SearchCountInput component", () => {
  render(
    <SearchCountInput
      setAmount={() => console.log(123)}
      loading={true}
      amount="2"
    />
  );
  const emptyComponentElement = screen.getByTestId("count-input");
  expect(emptyComponentElement).toBeInTheDocument();
  expect(emptyComponentElement.value).toBe("2");
  expect(emptyComponentElement.disabled).toBeTruthy();
});

test("should render SearchCountInput component with another props", () => {
  render(
    <SearchCountInput
      setAmount={() => console.log(123)}
      loading={false}
      amount="4"
    />
  );
  const emptyComponentElement = screen.getByTestId("count-input");
  expect(emptyComponentElement).toBeInTheDocument();
  expect(emptyComponentElement.value).toBe("4");
  expect(emptyComponentElement.disabled).toBeFalsy();
});
